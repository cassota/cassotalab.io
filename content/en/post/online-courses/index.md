---
title: Free Online Courses
date: 2020-06-29
lastmod: 2020-07-05
authors:
  - Cassio Batista
math: true
diagram: true
---

I decided to post some comments about some excellent online courses related to 
computing and engineering that I've started to take during Covid-19 
self-quarantine. Literally all of them are available **for free** on YouTube.

<!--more-->

## Index
- [WFST: Weighted Finite-State Transducers @ NTU](#weighted-finite-state-transducers)
- [Linux Basics for SysAdmins @ tutoriaLinux](#linux-basics-for-sysadmins)
- [Computer Networking Crash Course @ Geek's Lesson](#computer-networking-crash-course) 
- [Probability and Stochastic Processes @ UCalgary](#probability-and-stochastic-processes)
- [Introduction to Probability @ MIT](#introduction-to-probability)
- [Machine Learning & Deep Learning Fundamentals @ deeplizard](#ml-and-deep-learning-fundamentals)
- [Keras with Tensorflow Course @ freeCodeCamp](#keras-with-tensorflow-course)
- [TensorFlow 2.0 for Beginners @ freeCodeCamp](#tensorflow-20-for-beginners)
- :warning: [Introduction to Reinforcement Learning @ UCL & DeepMind](#introduction-to-reinforcement-learning)
- :warning: [Reinforcement Learning @ Stanford](#reinforcement-learning)
- :warning: [Natural Language Processing with Deep Learning @ Stanford](#nlp-with-deep-learning)
- :warning: [Applied Linear Algebra @ Uni of Nebraska](#applied-linear-algebra)

## Weighted Finite-State Transducers

:calendar: Enrollment: March 2020

This is of particular interest for those who are dealing with ASR (speech
recognition). Working with Kaldi, for example, one might bump into a `HCLG.fst`
file, which is, in an oversimplified way, the composition of the three main
resources for ASR: the lexicon `L`, the language model `G`, and the acoustic
model `H` with context-dependency `C`. Nanyang Technological University's 
[Lim Zhi Hao](https://www.linkedin.com/in/lim-zhi-hao-025546115/?originalSubdomain=sg)
provided a 13-video lecture series which gives a nice picture of the 
theoretical foundations of WFSTs and semirings notation.

[{{< icon name="university" pack="fas" >}}](https://www.ntu.edu.sg/Pages/home.aspx) _Nanyang Technological University_    
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/playlist?list=PLxbPHSSMPBeicXAHVfyFvGfCywRCq39Mp) _Semirings and WFST, 2015_

<!--
{{% alert note %}}
If you don't know anything at all about automata (e.g. you don't even know 
what a DFA is, then you might want to read something before engaging on WFSTs
lectures.
{{% /alert %}}
-->

## Linux Basics for SysAdmins

:calendar: Enrollment: Apr 2020

This is a phenomenal overview course on Linux of about 60 videos that as a
beginner-intermediate you can really learn a lot from. I work with command
line tools for about 8 years and some of the things it provides I haven't 
actually even heard about (e.g., the "script" command).  Thanks to tutoriaLinux
channel by [Dave Cohen](https://twitter.com/tutorialinux)!

[{{< icon name="hat-wizard" pack="fas" >}}](https://tutorialinux.com/) _tutoriaLinux_    
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/playlist?list=PLtK75qxsQaMLZSo7KL-PmiRarU7hrpnwK) _The Linux Basics Course: Beginner SysAdmin, Step by Step, 2014-today_

## Computer Networking Crash Course

:calendar: Enrollment: May 2020

This was a fortunate attempt to fill the huge gap from by my undergrad years.
This five-hour, single-take course is composed by six modules that provide a
solid foundation in computer network. The course is provide by Geek's Lesson
channel, and was migrated from Coursera's original course "IT Support 
Professional Certificate". This module is ministered by Victor Escobedo,
an engineer at Google.

[{{< icon name="hat-wizard" pack="fas" >}}](https://cslesson.org/) _Geek's Lesson_    
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/watch?v=QKfk7YFILws) _Computer Networking Complete Course - Beginner to Advanced_

## Probability and Stochastic Processes

:calendar: Enrollment: June 2020

This is the best course I've found online so far on the topic. University of
Calgary's Professor 
[Geoffrey Messier](https://schulich.ucalgary.ca/contacts/geoffrey-messier) did
a remarkable job on his thorough explanations regarding random variables and 
random processes in a 14-module, crash-course-like lecture series.

[{{< icon name="university" pack="fas" >}}](http://schulich.ucalgary.ca/contacts/geoffrey-messier) _Greoffrey Messier @ University of Calgary's Schulich School of Engineering_    
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/playlist?list=PL7sWxFnBVJLUbrCHertPLEqqCyLVnG-tN) _Probability and Stochastic Processes, 2018_

{{% alert note %}}
Prior knowledge on signals and systems is advised.
{{% /alert %}}

## Introduction to Probability

:calendar: Enrollment: July-Aug 2020

This course is ministered by Professor John Tsitsiklis is another gem provided
by MIT. I actually found three distinct playlists on YouTube: the original for
6.041 "Probabilistic Systems Analysis and Applied Probability" course; the
most up-to-date version, RES-6.012 on "Introduction to Probability", which was
developed specially to be taken as online classes; and the third one, 6.041SC,
which contains the very same videos as the original, plus some recitation 
videos where student TAs solve some exercises in between lectures.


[{{< icon name="university" pack="fas" >}}](https://ocw.mit.edu/index.htm) _MIT OpenCourseWare_    
[{{< icon name="archive" pack="fas" >}}](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-041-probabilistic-systems-analysis-and-applied-probability-fall-2010/) _MIT 6.041 (original 2010)_    
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/playlist?list=PLUl4u3cNGP60hI9ATjSFgLZpbNJ7myAg6) _MIT RES-6.012 (edX 2018)_    
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/playlist?list=PLUl4u3cNGP60A3XMwZ5sep719_nh95qOe) _MIT 6.041SC (original 2010 plus examples solved by TAs 2013)_

## ML and Deep Learning Fundamentals

:calendar: Enrollment: Aug 2020

This is a fantastic crash course from deeplizard on the most fundamental 
aspects of deep learning that might give you some enlightenment regarding Keras
functions and parameters from the sequential API. Moreover, it may make 
yourself comfortable with the concepts of activation functions, training and 
validation datasets, data augmentation, one-hot encoding, loss functions, 
backpropagation, overfitting vs. underfitting, maxpooling, feed-forward and 
convolutional networks, batches, regularization, transfer learning, etc.

[{{< icon name="hat-wizard" pack="fas" >}}](https://deeplizard.com/learn/video/gZmobeGL0Yg) _deeplizard: Machine Learning & Deep Learning Fundamentals_    
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/playlist?list=PLZbbT5o_s2xq7LwI2y8_QtvuXZedL6tQU) _Machine Learning & Deep Learning Fundamentals, 2017_


## Keras with Tensorflow Course

:calendar: Enrollment: Aug 2020

This is another course from deeplizard's Mandy which was made available as a 
single 3-hour-long video via freeCodeCamp. I think it was rather shortened or 
is incomplete but it is enough to get you going on classification and transfer 
learning with CNNs. The up-to-date version might be found at deeplizard's 
official YouTube channel or webpage.

[{{< icon name="hat-wizard" pack="fas" >}}](https://deeplizard.com/learn/video/RznKVRTFkBY) _deeplizard: Keras -- Python Deep Learning Neural Network API_     
[{{< icon name="hat-wizard" pack="fas" >}}](https://www.freecodecamp.org/news/keras-video-course-python-deep-learning/) _freeCodeCamp: Keras Course -- Learn Python DL and Neural Networks_     
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/watch?v=qFJeN9V1ZsI) _Keras with TensorFlow Course - Python DL for Beginners Tutorial, 2020_


## TensorFlow 2.0 for Beginners

:calendar: Enrollment: July--Aug 2020

This 7-hour-long course ministered by Tim Ruscica contains just the barebones
to get you going with most of the most-common learning algorithms in Keras. It
isn't very didactic though, so I recommend watching the lessons from deeplizard
before enrolling this one.

[{{< icon name="hat-wizard" pack="fas" >}}](https://techwithtim.net/) _Tech with Tim_    
[{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/watch?v=tPYj3fFJGjk) _TensorFlow 2.0 Complete Course - Python NN for Beginners Tutorial, 2020_

## Introduction to Reinforcement Learning

:calendar: Enrollment: May-Aug 2020

TBD. 

The course is ministered at University College London by David Silver, 
DeepMind's genius. [{{< icon name="youtube" pack="fab" >}}](https://www.youtube.com/playlist?list=PLqYmG7hTraZDM-OYHWgPebj2MfCFzFObQ)

{{% alert note %}}
Prior knowledge on probability and random processes is advised.
{{% /alert %}}

## Reinforcement Learning

:calendar: Enrollment: Aug 2020

TBD.


## NLP with Deep Learning

:calendar: Enrollment: Sept 2020

TBD.

## Applied Linear Algebra

:calendar: Enrollment: Sept 2020

TBD.

