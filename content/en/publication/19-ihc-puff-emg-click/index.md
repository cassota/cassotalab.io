---
title: "Evaluating Alternative Interfaces Based on Puff, Electromyography and Dwell Time for Mouse Clicking"
authors:
- Erick Campos
- Denis Martins
- Suzane dos Santos
- Renan Cunha
- Cassio Batista
- Nelson Neto
date: "2019-07-21T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2019-07-22T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *Proceedings of the XVIII Brazilian Symposium on Human Factors in Computing Systems (IHC'19)*
publication_short: In *Proc. of IHC 2019*

abstract: "Computer systems are approaching human behavior in the sense of performing similar tasks, such as listening, understanding, thinking, and speaking. Although the forms of interaction with these systems have also followed the path of technology evolution, computer mice and keyboards do continue to play the leading role as a bridge between human and computer. The design of these devices forces the user to use their hands and this represents a major problem that has been already recognized in the literature with several pro- posed solutions. Nevertheless, in spite of the various methods found, it is difficult to find papers that make comparisons between them. Therefore, this work proposes an evaluation, using quantitative and qualitative analyses, of three alternative methods for mouse click: dwell time, mouth-puffing and electromyography. As result, both analyses showed that the interactions based on mouth-puffing and electromyography performed better than the dwell time method."

# Summary. An optional shortened abstract.
summary: "Statistical comparison among three different types of mouse click: mouth puffing, EMG and dwell-time. Two out of these three methods have been developed in hardware and their schematics been open-sourced."

tags:
- Assistive Technology
- Mouth Puffing
- Electromyography
- Dwell Time
- Mouse Click
featured: false

links:
#url_pdf: https://sigaa.ufpa.br/sigaa/verProducao?idProducao=288308&key=f4167302e88055bdb128a86364573390
url_code: https://github.com/ErickCampos/At-Switch
#url_dataset: '#'
#url_poster: '#'
#url_project: https://hackaday.io/project/26830-tv-remote-control-based-on-head-gestures
#url_slides: ''
#url_source: '#'
#url_video: https://www.youtube.com/watch?v=RwNVhA8_5lQ

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

