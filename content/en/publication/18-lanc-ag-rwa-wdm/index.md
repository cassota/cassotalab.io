---
title: "Static-Traffic Routing and Wavelength Assignment in Transparent WDM Networks Using Genetic Algorithm"
authors:
- Cassio Batista
- Diego Teixeira
- Thiago Coelho
- Josivaldo Araújo
date: "2018-10-04T00:00:00Z"
doi: "10.1145/3277103.3277126"

# Schedule page publish date (NOT publication's date).
publishDate: "2018-10-04T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *Proceedings of the 10th Latin America Networking Conference (LANC'18)*
publication_short: In *Proc. of LANC'18*

abstract: In order to transmit data efficiently over an optical network, many routing and wavelength assignment (RWA) algorithms have been proposed. This work presents a genetic algorithm that aims at solving the RWA problem, which consists of choosing the most suitable lightpath (i.e., a combination of a route and a wavelength channel) between a source-destination pair of nodes in all-optical networks. A comparison to some already known approaches in terms of blocking probability per load over four network topologies was made. Simulation results show a good performance, since the average blocking probability achieved by the proposed genetic algorithm was relatively equivalent to the values yielded by the standard approaches over two network topologies; and way lower on the other two networks.  

# Summary. An optional shortened abstract.
summary: A genetic algorithm for solving the RWA problem, which consists of choosing the most suitable lightpath (i.e., a combination of a route and a wavelength channel) between a source-destination pair of nodes over four all-optical WDM network topologies.

tags:
- Routing and Wavelength Assignment
- Genetic Algorithm
- Optical Networks
featured: false

links:
url_pdf: https://dl.acm.org/citation.cfm?doid=3277103.3277126
url_code: https://github.com/cassiobatista/RWA-WDM
#url_dataset: '#'
#url_poster: poster.pdf
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

