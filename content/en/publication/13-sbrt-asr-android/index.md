---
title: "Desenvolvimento e Comparação De Reconhecedores De Fala Embarcados e Distribuídos Para Android"
authors:
- Cassio Batista
- Thiago Coelho
- Bruno Haick
- Nelson Neto
- Aldebaro Klautau
date: "2013-09-03T00:00:00Z"
doi: "10.14209/sbrt.2013.219"

# Schedule page publish date (NOT publication's date).
publishDate: "2013-09-03T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *XXXI Brazilian Telecommunications Symposium (SBrT'13)*
publication_short: In *Proc. of SBrT 2013*

abstract: "Este trabalho compara dois sistemas de reconhecimento de fala que podem ser usados no desenvolvimento de aplicativos para Android: Julius em modo servidor e Google. Parte do suporte a Português Brasileiro para o Julius foi desenvolvido pelos autores no contexto do projeto FalaBrasil. O Julius também utilizou o servidor do FalaBrasil para prover reconhecimento distribuído via Internet, de maneira similar ao sistema da empresa Google. São apresentadas comparações entre os mesmos em termos de taxa de acerto (acurácia) e custo computacional."

# Summary. An optional shortened abstract.
summary: "Comparação, em termos de taxa de acerto (acurácia) e custo computacional, entre dois sistemas de reconhecimento de fala que podem ser usados no desenvolvimento de aplicativos para Android: Julius em modo servidor e Google."

tags:
- Automatic Speech Recogntion
- Android
- Google
- PocketSphinx
- Brazilian Portuguese
featured: false

links:
url_pdf: http://gestao.sbrt.org.br/simposios/artigo/download/a/325
#url_code:
#url_dataset: '#'
#url_poster: poster.pdf
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'eita'
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
