+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Speech processing research"
  company = "Vivoka"
  company_url = "https://www.vivoka.com"
  location = "Metz, France"
  date_start = "2023-06-12"
  date_end = ""
  description = """Speech recognition"""

[[experience]]
  title = "Speech processing research"
  company = "CPqD"
  company_url = "https://www.cpqd.com.br/"
  location = "Campinas, Brazil"
  date_start = "2021-03-02"
  date_end = "2023-05-12"
  description = """Speech-based technologies: 

  * Lattice rescoring via n-grams and neural networks LM
  * ASR + VAD and SER (emotion)
  """

[[experience]]
  title = "PhD in Computer Science"
  company = "Federal University of Pará (UFPA)"
  company_url = "https://portal.ufpa.br/"
  location = "Belém, Brazil"
  date_start = "2017-12-07"
  date_end = "2022-10-07"
  description = """Speech-based technologies: 

  * Kaldi ASR for Brazilian Portuguese 
  * Utterance copy TTS in English using Klatt and deep learning techniques
  """

[[experience]]
  title = "MSc in Computer Science"
  company = "Federal University of Pará (UFPA)"
  company_url = "https://portal.ufpa.br/"
  location = "Belém, Brazil"
  date_start = "2017-03-01"
  date_end = "2017-12-07"
  description = """A universal remote control system in C++ for people with upper-limb motor disabilities, so they could control a TV via alternative methods. 

  * OpenCV for head gesture recognition  
  * PocketSphinx for speech recognition  
  * Adaptive switches in hardware 
  """

[[experience]]
  title = "Research Internship"
  company = "Embrapa"
  company_url = "https://www.embrapa.br/en/international"
  location = "Belém, Brazil"
  date_start = "2016-03-01"
  date_end = "2016-12-31"
  description = """A simulator in Python for the routing and wavelength assignment (RWA) problem over transparent, wavelength-multiplexed optical networks using Genetic Algorithms."""

[[experience]]
  title = "Summer Internship"
  company = "Óbuda University (OE)"
  company_url = "http://uni-obuda.hu/"
  location = "Budapest, Hungary"
  date_start = "2014-03-01"
  date_end = "2015-01-05"
  description = """Development of speech (English) modules for controlling Teki: a personal home assistant, Turtlebot-based robot
    
  * PocketSphinx desktop on Linux + ROS (offline)
  * Android's Google ASR (online Wi-Fi UDP connection)
  """

[[experience]]
  title = "Research Internship"
  company = "Federal University of Pará (UFPA)"
  company_url = "https://portal.ufpa.br/"
  location = "Belém, Brazil"
  date_start = "2012-01-01"
  date_end = "2016-02-29"
  description = """Development of resources and applications for spech recognition in Brazilian Portuguese:

  * PyQt4 CFG/BNF grammar tester for Julius
  * Acoustic model training on CMU Sphinx for KDE Simon Listens
  * Android client + Julius server vs. Google's Android ASR
  """


+++
