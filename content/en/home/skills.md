+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Skills"
subtitle = "What am I (supposed to be) good at?"

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

[[feature]]
  icon = "microphone"
  icon_pack = "fas"
  name = "<a href='https://github.com/falabrasil'>Speech Recognition</a>"
  description = "Kaldi, Icefall (K2), SpeechBrain, etc."

[[feature]]
  icon = "linux"
  icon_pack = "fab"
  name = "Linux & Tools"
  description = "Arch, XMonad, Vim, Git, Python, C, etc."

[[feature]]
  icon = "brain"
  icon_pack = "fas"
  name = "Machine Learning"
  description = "PyTorch, Scikit-learn, ONNX, etc."

+++
