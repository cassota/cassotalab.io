---
title: "ASR BBB"
summary: "Speech recognition and TV remote control using Android and BeagleBone Black"
authors: [Cassio Batista]
date: 2015-12-01
---

## Beaglebone Black and Speech Recognition

This is just a silly attempt to provide some online documentation for my
undergraduation term paper (mostly from 2015) which used a client-server model
to control a TV set via speech commands. The client module runs over an Android
smartphone and the server uses Julius as backend over a Beaglebone Black board
running a Debian-based Linux. There is nothing properly documented in English
so I recommend you to follow another one of my projects called 
[ASR Remote](https://cassota.gitlab.io/project/asr-remote/). The Android source
code can be found in this link too.

## Demo

<iframe width="720" height="480" src="https://www.youtube.com/embed/vY7kQsz_mHw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Useful Links

- ["{{< icon name="file-pdf" pack="fas" >}}Uma Proposta de Sistema de Controle Remoto Universal com Suporte a Reconhecimento e Síntese de Voz"](./main_tcc.pdf):
  this is my undergrad thesis, but it unfortunatelly is written in Brazilian
  Portuguese. Most of the code, on the other hand, is scattered across many of
  my GitHub repos.
- ["{{< icon name="file-pdf" pack="fas" >}}Uso de Reconhecedor e Sintetizador de Voz Embarcados para Controle de Equipamentos Eletrônicos via Luz Infravermelha"](./tech_report_II.pdf):
  this is the main documentation written for the project "as it is" during my
  undergrad, also written in Brazilian Portuguese. It comes with a 
  [presentation PDF](./present.pdf) as well.
- https://github.com/cassiobatista/Beagle-Beagle. 
  Here you may find the ASR server built over Julius decoder.
