---
title: "Head Remote"
summary: "A system where user's head gestures are translated into remote commands to electronic devices"
authors: [Cassio Batista]
date: 2020-04-19
---

This project presents an open-source, low cost universal remote control system
that translates user’s head poses into commands to electronic devices. Among
the applications found with respect to alternative remote controls, none of
them supports head gestures as input, which would certainly make them a viable
option to people whose upper limbs are compromised.

## Demo

<iframe width="720" height="480" src="https://www.youtube.com/embed/RwNVhA8_5lQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Useful Links

- Documentation: for details about this project, please refer to the more- 
  complete documentation I wrote a couple of years ago on 
  [Hackaday](https://hackaday.io/project/26830-tv-remote-control-based-on-head-gestures). 
  Codes and resources are on my personal [GitHub](https://github.com/cassiobatista/hpe-remote).
- This was also part of my [master's thesis](http://www.ppgcc.propesp.ufpa.br/Disserta%C3%A7%C3%B5es_2017/Cassio%20Trindade%20Batista_Disserta%c3%a7%c3%a3o.pdf).
- Citation: if you use the scripts from GitHub, please do cite my paper as well: 
  **["A Proposal of a Universal Remote Control System Based on Head Movements"](/publication/17-ihc-hpe-remote/)**.
