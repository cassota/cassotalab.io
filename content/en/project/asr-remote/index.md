---
title: "Speech Remote"
summary: "A remote control system that translates the user's spoken words into commands to electronic devices"
authors: [Cassio Batista]
date: 2018-08-20
---

This project presents an open-source, low cost universal remote control system 
that translates user’s speech into commands to electronic devices. The system 
works well in both English and Brazilian Portuguese languages. Although cloud 
speech recognition systems are getting really usual, the good thing about the 
project is that it uses a local, offline speech recognition system, which 
doesn't require a connection to the Internet. Plus, controlling things via 
speech would certainly be a viable option to people whose upper limbs are 
compromised.

## Demo

<iframe width="720" height="480" src="https://www.youtube.com/embed/6phJRVuzp0c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Useful Links

- Documentation: for details about this project, please refer to the more- 
  complete documentation I wrote a couple of years ago on 
  [Hackaday](https://hackaday.io/project/34317-tv-remote-control-via-offline-speech-recognition). 
  Codes and resources are on my personal [GitHub](https://github.com/cassiobatista/asr-remote).
- This was also part of my [master's thesis](http://www.ppgcc.propesp.ufpa.br/Disserta%C3%A7%C3%B5es_2017/Cassio%20Trindade%20Batista_Disserta%c3%a7%c3%a3o.pdf) 
  (in Brazilian Portuguese).
- Citation: if you use the Sphinx resources from the 
  [FalaBrasil Group](https://ufpafalabrasil.gitlab.io/), 
  for performing speech recognition in Brazilian Portuguese, please do cite my 
  paper as well: 
  **["Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools"](/publication/18-iberspeech-asr-kaldi/)**.
