---
# Display name
name: Cassio T Batista

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Speech processing researcher

# Organizations/Affiliations
organizations:
- name: Grupo FalaBrasil
  url: "https://ufpafalabrasil.gitlab.io/" 

# Short bio (displayed in user profile at end of posts)
bio: "Interested in speech processing"

interests:
- Speech processing

education:
  courses:
  - course: PhD in Computer Science
    institution: Federal University of Pará
    year: 2023
  - course: MSc in Computer Science
    institution: Federal University of Pará
    year: 2017
  - course: BSc in Computer Engineering
    institution: Federal University of Pará
    year: 2016

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: "lattes"
  icon_pack: "ai"
  link: "http://lattes.cnpq.br/2464594478834482"
- icon: "google-scholar"
  icon_pack: "ai"
  link: "https://scholar.google.com.br/citations?user=_04j8QUAAAAJ"
#- icon: "researchgate"
#  icon_pack: "ai"
#  link: "https://www.researchgate.net/profile/Cassio_Batista"
- icon: "linkedin"
  icon_pack: "fab"
  link: "https://linkedin.com/in/cassiotbatista"
- icon: "github"
  icon_pack: "fab"
  link: "https://github.com/cassiotbatista"
- icon: "skull"
  icon_pack: "fas"
  link: "https://hackaday.io/cassota"
#- icon: "youtube"
#  icon_pack: "fab"
#  link: "https://www.youtube.com/channel/UCWPJAKVyu7Kv4LBk6-ngraQ"
- icon: "twitter"
  icon_pack: "fab"
  link: "https://twitter.com/cassiotbatista"
- icon: "envelope"
  icon_pack: "far"
  link: "#contact"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
#- icon: "address-card"
#  icon_pack: "far"
#  link: "files/cv_cassio.pdf"
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

This is a legacy website. Please refer to
[@cassiotbatista](https://cassiotbatista.github.io/) instead.

I have a PhD degree in Computer Science (2023) conferred by Federal University
of Pará (UFPA) in Belém, Brazil. Currently, I am doing research in speech
processing at Vivoka in Metz, France. My professional experience includes
mostly speech recognition and machine learning.
