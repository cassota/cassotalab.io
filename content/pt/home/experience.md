+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experiência Profissional"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Pesquisa em processamento de fala"
  company = "Vivoka"
  company_url = "https://www.vivoka.com"
  location = "Metz, França"
  date_start = "2023-06-12"
  date_end = ""
  description = """Reconhecimento de fala"""

[[experience]]
  title = "Pesquisa em processamento de fala"
  company = "CPqD"
  company_url = "https://www.cpqd.com.br/"
  location = "Campinas, São Paulo"
  date_start = "2021-03-02"
  date_end = "2023-05-12"
  description = """Tecnologias baseadas em fala: 

  * Lattice rescoring via LMs com n-grams e redes neurais
  * ASR + VAD e SER (emoção)
  """

[[experience]]
  title = "Doutorado em Ciência da Computação"
  company = "Universidade Federal do Pará (UFPA)"
  company_url = "https://portal.ufpa.br/"
  location = "Belém do Pará"
  date_start = "2017-12-07"
  date_end = "2022-10-07"
  description = """Tecnologias baseadas em fala: 

  * Reconhecimento de fala em Português Brasileiro com o Kaldi
  * Imitação da voz humana (síntese) em Inglês com o Klatt e técnicas de aprendizado profundo
  """

[[experience]]
  title = "Mestrado em Ciência da Computação"
  company = "Universidade Federal do Pará (UFPA)"
  company_url = "https://portal.ufpa.br/"
  location = "Belém do Pará"
  date_start = "2017-03-01"
  date_end = "2017-12-07"
  description = """Sistema de controle remoto universal em C++ para que pessoas com deficiência motora dos memberos superiores consigam controlar uma TV via métodos alternativos. 

  * OpenCV para reconhecimento de gestos da cabeça
  * PocketSphinx para reconhecimento de fala
  * Acionadores externos em hardware
  """

[[experience]]
  title = "Pesquisador Auxiliar"
  company = "Embrapa"
  company_url = "https://www.embrapa.br/en/international"
  location = "Belém do Pará"
  date_start = "2016-03-01"
  date_end = "2016-12-31"
  description = """Simulador em Python para roteamento e alocação de comprimento de onda (RWA) em redes ópticas WDM transparentes utilizando algoritmos genéticos."""

[[experience]]
  title = "Estágio de Verão"
  company = "Universidade de Óbuda (OE)"
  company_url = "http://uni-obuda.hu/"
  location = "Budapeste, Hungria"
  date_start = "2014-03-01"
  date_end = "2015-01-05"
  description = """Módulos de reconhecimento de fala (em Inglês) para controlar o Teki: um robô assistente pessoal baseado na plataforma Turtlebot
    
  * PocketSphinx no desktop Linux + ROS (offline)
  * Google ASR no Android (online por conexão UDP via Wi-Fi)
  """

[[experience]]
  title = "Bolsista de Iniciação Científica"
  company = "Universidade Federal do Pará (UFPA)"
  company_url = "https://portal.ufpa.br/"
  location = "Belém do Pará"
  date_start = "2012-01-01"
  date_end = "2016-02-29"
  description = """Recursos e aplicações para reconhecimento de fala em Português Brasileiro:

  * Interface em PyQt4 para testar gramáticas GLC/BNF no Julius
  * Treino de modelo acústico no CMU Sphinx para o Simon Listens KDE
  * Cliente Android + Julius servidor vs. Google ASR no Android
  """

+++
