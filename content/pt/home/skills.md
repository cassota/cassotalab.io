+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Habilidades"
subtitle = "O que eu (supostamente) sei fazer?"

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

[[feature]]
  icon = "microphone"
  icon_pack = "fas"
  name = "<a href='https://github.com/falabrasil'>Reconhecimento de Fala</a>"
  description = "Kaldi, Icefall (K2), SpeechBrain, etc."

[[feature]]
  icon = "linux"
  icon_pack = "fab"
  name = "Linux & Outros"
  description = "Arch, XMonad, Vim, Git, Python, C, etc."

[[feature]]
  icon = "brain"
  icon_pack = "fas"
  name = "Aprendizado de Máquina"
  description = "PyTorch, Scikit-learn, ONNX, etc."

+++
