---
title: "LaPS CSR: A Free Distributed Cloud Speech Recognition System"
authors:
- Cassio Batista
- Thiago Coelho
- Bruno Haick
- Nelson Neto
- Aldebaro Klautau
date: "2014-02-03T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2014-03-03T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *XIX International Scientific Conference for Young Engineers (FMTU'14)*
publication_short: In *Proc. of 19th FMTU 2014*

abstract: This paper describes a cloud speech recognition service based on Julius decoder running in server mode. The system was set up to recognize speech in Brazilian Portuguese. The support to the language was developed by the authors with FalaBrasil research group tools, which are free and available on the group's site. Julius uses the FalaBrasil cloud to provide online and distributed speech recognition via Internet. The client side was built on the Android 2.2 platform. The application can record and send audio, detect the end of the user speech and listen to the decoder result. To test the system efficiency, recognition time and accuracy rate were estimated by comparing it to SpeechRecognizer API provided by Google.

# Summary. An optional shortened abstract.
summary: This paper describes a cloud speech recognition service in Brazilian Portuguese based on Julius decoder running in server mode. The client side was built on the Android 2.2 platform. 

tags:
- Automatic Speech Recogntion
- Android
- Client-server
- Google
- Julius
- Brazilian Portuguese
featured: false

links:
url_pdf: https://cassota.gitlab.io/publication/14-fmtu-lapscsr/paper.pdf
url_code: http://hdl.handle.net/10598/28297
#url_dataset: '#'
#url_poster: 
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'eita'
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
