---
title: "Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools"
authors:
- Cassio Batista
- Larissa Dias
- Nelson Neto
date: "2018-11-21T00:00:00Z"
doi: "10.21437/IberSPEECH.2018-17"

# Schedule page publish date (NOT publication's date).
publishDate: "2018-11-21T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *Proceedings of IberSPEECH 2018*
publication_short: In *Proc. IberSPEECH 2018*

abstract: Kaldi has become a very popular toolkit for automatic speech recognition, showing considerable improvements through the combination of hidden Markov models (HMM) and deep neural networks (DNN). However, in spite of its great performance for some languages (e.g. English, Italian, Serbian, etc.), the resources for Brazilian Portuguese (BP) are still quite limited. This work describes what appears to be the first attempt to create Kaldi-based scripts and baseline acoustic models for BP using Kaldi tools. Experiments were carried out for dictation tasks and a comparison to CMU Sphinx toolkit in terms of word error rate (WER) was performed. Results seem promising, since Kaldi achieved the absolute lowest WER of 4.75% with HMM-DNN and outperformed CMU Sphinx even when using Gaussian mixture models only.

# Summary. An optional shortened abstract.
summary: The first attempt to create scripts and baseline acoustic models for Brazilian Portuguese using Kaldi tools. 

tags:
- Automatic Speech Recognition
- Deep Neural Networks
- Kaldi
- CMU Sphinx
- Brazilian Portuguese
featured: true

links:
url_pdf: https://www.isca-speech.org/archive/IberSPEECH_2018/pdfs/IberS18_P1-13_Batista.pdf
url_code: https://gitlab.com/fb-asr/fb-am-tutorial/kaldi-am-train/
#url_dataset: '#'
url_poster: poster.pdf
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

