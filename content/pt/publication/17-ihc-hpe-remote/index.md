---
title: "A Proposal of a Universal Remote Control System Based on Head Movements"
authors:
- Cassio Batista
- Erick Campos
- Nelson Neto
date: "2017-10-21T00:00:00Z"
doi: "10.1145/3160504.3160516"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-10-21T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *Proceedings of the XVI Brazilian Symposium on Human Factors in Computing Systems (IHC'17)*
publication_short: In *Proc. of IHC 2017*

abstract: "Technological developments converge to make people interact with electronic devices in an easy way. For people with disabilities, however, that interaction becomes something more than simple: it becomes possible. The current work presents a proposal of an open-source, low cost universal remote control system that translates user's head poses into commands to electronic devices. In addition, a proximity sensor circuit was combined to radio-frequency modules in order to act as a wireless switch. Among the applications found with respect to alternative remote controls, none of them supports head gestures as input, which would certainly make them a viable option to people whose upper limbs are compromised. A mean opinion score questionnaire was applied to volunteers in order to evaluate the system. The results show great interest of the users in this kind of technology."

# Summary. An optional shortened abstract.
summary: An open-source, low cost universal remote control system that translates user's head poses into commands to electronic devices. In addition, a proximity sensor circuit was combined to radio-frequency modules in order to act as a wireless switch.

tags:
- Head Pose Estimation
- Gesture Recognition
- Assistive Technology
- Remote Control
featured: true

links:
url_pdf: https://sigaa.ufpa.br/sigaa/verProducao?idProducao=288308&key=f4167302e88055bdb128a86364573390
url_code: https://github.com/cassiobatista/hpe-remote
#url_dataset: '#'
#url_poster: '#'
url_project: https://hackaday.io/project/26830-tv-remote-control-based-on-head-gestures
#url_slides: ''
#url_source: '#'
url_video: https://www.youtube.com/watch?v=RwNVhA8_5lQ

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

