---
title: "Improving Metagenomic Assemblies Through Data Partitioning: A GC Content Approach"
authors:
- Fábio Miranda
- Cassio Batista
- Artur Silva
- Jefferson Morais
- Nelson Neto
- Rommel Ramos
date: "2018-03-28T00:00:00Z"
doi: "10.1007/978-3-319-78723-7_36"

# Schedule page publish date (NOT publication's date).
publishDate: "2018-03-28T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *Bioinformatics and Biomedical Engineering (IWBBIO 2018)*
publication_short: In *Proc. of IWBBIO 2018*

abstract: "Assembling metagenomic data sequenced by NGS platforms poses significant computational challenges, especially due to large volumes of data, sequencing errors, and variations in size, complexity, diversity and abundance of organisms present in a given metagenome. To overcome these problems, this work proposes an open-source, bioinformatic tool called GCSplit, which partitions metagenomic sequences into subsets using a computationally inexpensive metric: the GC content. Experiments performed on real data show that preprocessing short reads with GCSplit prior to assembly reduces memory consumption and generates higher quality results, such as an increase in the size of the largest contig and N50 metric, while both the L50 value and the total number of contigs produced in the assembly were reduced. GCSplit is available at https://github.com/mirand863/gcsplit."

# Summary. An optional shortened abstract.
summary: "GCSplit: an open-source, bioinformatic tool which partitions metagenomic sequences into subsets using a computationally inexpensive metric: the GC content."

tags:
- DNA Sequencing
- Bioinformatics
- DNA Sequencing
- Metagenomics
- Data Partitioning
featured: false

links:
url_pdf:  https://www.biorxiv.org/content/biorxiv/early/2018/02/08/261784.full.pdf
url_code: https://github.com/mirand863/gcsplit
#url_dataset: '#'
#url_poster: poster.pdf
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'eita'
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
