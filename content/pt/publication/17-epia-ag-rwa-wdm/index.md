---
title: "A Genetic Algorithm Approach for Static Routing and Wavelength Assignment in All-Optical WDM Networks"
authors:
- Diego Teixeira
- Cassio Batista
- Afonso Cardoso
- Josivaldo Araújo
date: "2017-08-09T00:00:00Z"
doi: "10.1007/978-3-319-65340-2_35"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-08-09T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *18th EPIA Conference on Artificial Inteligence - Encontro Português de Inteligência Artificial*,
publication_short: In *Progress in Artificial Intelligence (EPIA'17)*

abstract: In order to transmit data efficiently over an optical network, many routing and wavelength assignment (RWA) algorithms have been proposed. This work presents a genetic algorithm that aims at solving the RWA problem, which consists of choosing the most suitable lightpath (i.e., a combination of a route and a wavelength channel) between a source-destination pair of nodes in all-optical networks. A comparison to some already known approaches in terms of blocking probability was made. Results show a reasonable performance, since the average blocking probability achieved by the genetic algorithm was lower than or relatively equivalent to the standard approaches compared.

# Summary. An optional shortened abstract.
summary: A genetic algorithm that aims at solving the RWA problem, which consists of choosing the most suitable lightpath (i.e., a combination of a route and a wavelength channel) between a source-destination pair of nodes in all-optical networks.

tags:
- Routing and Wavelength Assignment
- Genetic Algorithm
- Optical Networks
featured: true

links:
url_pdf: https://link.springer.com/chapter/10.1007%2F978-3-319-65340-2_35
url_code: https://github.com/cassiobatista/RWA-WDM
#url_dataset: '#'
#url_poster: poster.pdf
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'eita'
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

