---
title: "A Parallel Strategy for a Genetic Algorithm in Routing Wavelength Assignment Problem Using GPU with CUDA"
authors:
- Esdras La-Roque
- Cassio Batista
- Josivaldo Araújo
date: "2020-08-15T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2020-08-26T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *Proceedings of 16th Encontro Nacional de Inteligência Artificial e Computacional 2020*
publication_short: In *Proc. ENIAC 2020*

abstract: "This paper presents a parallel strategy with a heuristic approach to reduce the execution time bottleneck of a routing and wavelength assignment problem in wavelength-division multiplexing networks of a previous work that uses a sequential genetic algorithm. As the parallelization solution, the GPU hardware processing on CUDA architecture and CUDA C programming language were adopted. The results achieved were between 35 and 40 times faster than the sequential version of the genetic algorithm."


# Summary. An optional shortened abstract.
summary: Routing and wavelength assingment simulador on NVIDIA CUDA GPUs.

tags:
- Routing and Wavelength Assignment
- Parallel Genetic Algorithm
- CUDA
featured: false

links:
url_pdf: https://sol.sbc.org.br/index.php/eniac/article/view/12175
url_code: https://github.com/cassiobatista/rwa-wdm-sim
#url_dataset: '#'
#url_poster: 
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

