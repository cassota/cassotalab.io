---
title: "Towards a Free, Forced Phonetic Aligner for Brazilian Portuguese Using Kaldi Tools"
authors:
- Ana Larissa Dias
- Cassio Batista
- Daniel Santana
- Nelson Neto
date: "2020-10-13T00:00:00Z"
doi: "10.1007/978-3-030-61377-8_44"

# Schedule page publish date (NOT publication's date).
publishDate: "2020-10-13T00:00:00Z"

# Publication type.
# Legend: 
#   0 = Uncategorized; 
#   1 = Conference paper;
#   2 = Journal article;
#   3 = Preprint / Working Paper; 
#   4 = Report; 
#   5 = Book; 
#   6 = Book section;
#   7 = Thesis; 
#   8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: In *Proceedings of 9th Brazilian Conference on Intelligent Systems 2020*
publication_short: In *Proc. BRACIS 2020*

abstract: "Phonetic analysis of speech, in general, requires the alignment of audio samples to its phonetic transcription. This task could be performed manually for a couple of files, but as the corpus grows large it becomes unfeasibly time-consuming, which emphasizes the need for computational tools that perform such speech-phonemes forced alignment automatically. Therefore, due to the scarce availability of phonetic alignment tools for Brazilian Portuguese (BP), this work describes the evolution process towards creating a free phonetic alignment tool for BP using Kaldi, a toolkit that has been the state of the art for open-source speech recognition. Five acoustic models were trained with Kaldi and tested in phonetic alignment, where the evaluation took place in terms of the phone boundary metric. The results show that its performance is similar to some Kaldi-based aligners for other languages, and superior to an outdated phonetic aligner for BP based on HTK toolkit."

# Summary. An optional shortened abstract.
summary: Forced phonetic alignment in Brazilian Portuguese using Kaldi tools.

tags:
- Phonetic alignment
- Acoustic modelling
- Brazilian Portuguese
- Kaldi
featured: false

links:
url_pdf: https://link.springer.com/chapter/10.1007%2F978-3-030-61377-8_44
url_code: https://gitlab.com/fb-align/kaldi-align/
#url_dataset: '#'
#url_poster: 
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: ""
  preview_only: true

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []
#- internal-project []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

