---
title: "Speech Remote"
summary: "Um sistema de controle remoto que converte fala em comandos para equipamentos eletrônicos"
authors: [Cassio Batista]
date: 2018-08-20
---

Este projeto apresenta um sistema de controle universal, de baixo custo e
código aberto que converte a fala do usuário em comandos para aparelhos
eletrônicos. O sistema funciona bem tanto em Inglês quanto em Português
Brasileiro. Apesar de soluções online em cloud terem se tornado
bastante populares, este projeto usa um sistema local para reconhecimento de
fala offline, o que não requer conexão com a Internet. 

## Demonstração

<iframe width="720" height="480" src="https://www.youtube.com/embed/6phJRVuzp0c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Links Úteis

- Documentação: a mais completa documentação que escrevi há alguns anos
  encontra-se inglês no blog do 
  [Hackaday](https://hackaday.io/project/34317-tv-remote-control-via-offline-speech-recognition). 
  Códigos-fonte e outros recursos estão no meu [GitHub](https://github.com/cassiobatista/asr-remote).
- Esse sistema também foi parte da minha [dissertação de mestrado](http://www.ppgcc.propesp.ufpa.br/Disserta%C3%A7%C3%B5es_2017/Cassio%20Trindade%20Batista_Disserta%c3%a7%c3%a3o.pdf).
- Citação: Caso os recursos do
  [Grupo FalaBrasil](https://ufpafalabrasil.gitlab.io/) para o CMU Sphinx
  funcionar em Português Brasileiro sejam utilizados em trabalhos acadêmicos, 
  favor citar o seguinte artigo:
  **["Baseline Acoustic Models for Brazilian Portuguese Using Kaldi Tools"](/publication/18-iberspeech-asr-kaldi/)**.
