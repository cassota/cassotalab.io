---
title: "ASR BBB"
summary: "Reconhecimento de fala e controle remoto de TV via Android e BeagleBone Black"
authors: [Cassio Batista]
date: 2015-12-01
---

## Beaglebone Black e Reconhecimento de Fala

Documentação online do meu TCC (de 2015/2016), onde um sistema baseado no
modelo cliente-servidor foi utilizado para controlar uma TV através de comandos
por voz. O módulo cliente é executado em um _smartphone_ Android, enquanto o
servidor utiliza a prória _engine_ do decodificador Julius como _backend,
tendo este sido configado em uma Beaglebone Black, plataforma embarcada baseada
na distribuição Debian_ do Linux.

## Demonstração

<iframe width="720" height="480" src="https://www.youtube.com/embed/vY7kQsz_mHw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Links Úteis

Esse projeto é antigo e os recursos (código, documentação, etc.) é meio
escassa. Porém há um mais recente chamado
[ASR Remote](https://cassota.gitlab.io/project/asr-remote/). O código fonte
para o Android pode ser encontrado no mesmo link também.

- ["{{< icon name="file-pdf" pack="fas" >}}Uma Proposta de Sistema de Controle Remoto Universal com Suporte a Reconhecimento e Síntese de Voz"](./main_tcc.pdf):
  este é o meu TCC completo. A maioria dos códigos está infelizmente espalhada
  por muitos repositórios.
- ["{{< icon name="file-pdf" pack="fas" >}}Uso de Reconhecedor e Sintetizador de Voz Embarcados para Controle de Equipamentos Eletrônicos via Luz Infravermelha"](./tech_report_II.pdf):
  este é a principal documentação de quando o projeto foi inicialmente
  concebido, portanto é a mais "oficial" que existe. Há também os
  [slides](./present.pdf) desse mesmo projeto de quando foi apresentado no 
  período da graduação.
- https://github.com/cassiobatista/Beagle-Beagle. 
  Here you may find the ASR server built over Julius decoder.
