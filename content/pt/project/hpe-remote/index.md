---
title: "Head Remote"
summary: "Um sistema onde os gestos da cabeça são traduzidos em comandos para equipamentos eletrônicos"
authors: [Cassio Batista]
date: 2020-04-19
---

Este projeto apresenta um sistema de controle universal, de baixo custo e
código aberto, que converte os gestos (poses) da cabeça do usuário em comandos
para dispositivos eletrônicos.

## Demonstração

<iframe width="720" height="480" src="https://www.youtube.com/embed/RwNVhA8_5lQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Links Úteis

- Documentação: a mais completa documentação que escrevi há alguns anos
  encontra-se inglês no blog do 
  [Hackaday](https://hackaday.io/project/26830-tv-remote-control-based-on-head-gestures). 
  Códigos-fonte e outros recursos estão no meu [GitHub](https://github.com/cassiobatista/hpe-remote).
- Esse sistema também foi parte da minha [dissertação de mestrado](http://www.ppgcc.propesp.ufpa.br/Disserta%C3%A7%C3%B5es_2017/Cassio%20Trindade%20Batista_Disserta%c3%a7%c3%a3o.pdf).
- Citação: caso os códigos desse sistema do GitHub sejam usados, favor citar o
  seguinte artigo: 
  **["A Proposal of a Universal Remote Control System Based on Head Movements"](/publication/17-ihc-hpe-remote/)**.
