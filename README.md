# [Academic Kickstart](https://sourcethemes.com/academic/)

**Academic** makes it easy to create a beautiful website for free using Markdown, Jupyter, or RStudio. Customize anything on your site with widgets, themes, and language packs. [Check out the latest demo](https://academic-demo.netlify.com/) of what you'll get in less than 10 minutes, or [view the showcase](https://sourcethemes.com/academic/#expo).

## Install

You can choose from one of the following four methods to install:

- [**one-click install using your web browser (recommended)**](https://sourcethemes.com/academic/docs/install/#install-with-web-browser)
- [install on your computer using **Git** with the Command Prompt/Terminal app](https://sourcethemes.com/academic/docs/install/#install-with-git)
- [install on your computer by downloading the **ZIP files**](https://sourcethemes.com/academic/docs/install/#install-with-zip)
- [install on your computer with **RStudio**](https://sourcethemes.com/academic/docs/install/#install-with-rstudio)

Then [personalize your new site](https://sourcethemes.com/academic/docs/get-started/).

### :fox_face: On GitLab with CI/CD Config
1. create an empty repo named `cassota.gitlab.io`
2. clone it: `git clone git@gitlab.com:cassota/cassota.gitlab.io.git`
3. fork academic kickstart locally:
    - set an 'upstream' origin to the kickstarter official github repo: `git remote add upstream https://github.com/sourcethemes/academic-kickstart`
    - pull the github commits into gitlab's upstream: `git pull upstream master`
4. init hugo academic theme as a submodule: `git submodule update --init --recursive`
5. set the baseurl in `config.toml` to `https://cassota.gitlab.io`
6. create gitlab's CI/CD config file ([copy and paste from here](https://gitlab.com/pages/hugo/blob/master/.gitlab-ci.yml))
7. change the hugo docker image path to `registry.gitlab.com/pages/hugo/hugo_extended:latest`
8. commit and push kickstarter and your own commits into your master branch: `git push origin master`

## Ecosystem

* **[Academic Admin](https://github.com/sourcethemes/academic-admin):** An admin tool to import publications from BibTeX or import assets for an offline site
* **[Academic Scripts](https://github.com/sourcethemes/academic-scripts):** Scripts to help migrate content to new versions of Academic

## License

Copyright 2017-present [George Cushen](https://georgecushen.com).

Released under the [MIT](https://github.com/sourcethemes/academic-kickstart/blob/master/LICENSE.md) license.

[![Analytics](https://ga-beacon.appspot.com/UA-78646709-2/academic-kickstart/readme?pixel)](https://github.com/igrigorik/ga-beacon)
